init();

function init() {

  document.getElementById('mobile').addEventListener("click", function () {
    resizeDocument("mobile");
  });
  document.getElementById('tablet').addEventListener("click", function () {
    resizeDocument("tablet");
  });
  document.getElementById('desktop').addEventListener("click", function () {
    resizeDocument("desktop");
  });
  document.getElementById('toggle').addEventListener("click", function () {
    toggleImages();
  });

  document.getElementById('screenshot').addEventListener("click", function () {
    var iframeURL = $(iframe).attr('src');

    window.open(iframeURL);
  });

  ajax("links.html", "links", function () {
    var items = document.getElementsByClassName("email-link");
    for (i = 0; i < items.length; i++) {
      var item = items[i];
      item.addEventListener("click", function () {
        var url = this.getAttribute('data-link') + "/index.html";
        updateiFrame(url);
        cachePage(url);
      })
    }
  });

  loadCachedPage();
  applyVendor();
}

function applyVendor() {
  Split(['#sidebar', '#content'], {
    sizes: [25, 75]
  });
  SimpleScrollbar.initEl(document.getElementById("sidebar"));
}

function cachePage(url) {
  localStorage.setItem("email_pageName", url);
}

function loadCachedPage() {
  if (localStorage.email_pageName !== "undefined") {
    updateiFrame(localStorage.email_pageName);
  }
}

function updateiFrame(url) {
  document.getElementById('iframe').src = url;
}

function toggleImages() {
  var match = "_noVisual";
  var frame = document.getElementById('iframe').contentWindow.document;
  var images = frame.getElementsByTagName('img');
  for (i = 0; i < images.length; i++) {
    var image = images[i];
    var imageUrl = image.src;
    var path = imageUrl.substring(imageUrl.lastIndexOf('/') + 1);
    if (path.includes(match)) {
      image.src = path.replace(match, '');
    } else {
      image.src = match + path;
    }
  }
}

function resizeDocument(size) {
  var frame = document.getElementById('iframe');

  if (size == "mobile") {
    frame.style.width = "375px";
  } else if (size == "tablet") {
    frame.style.width = "580px";
  } else {
    frame.style.width = "100%";
  }
}

function ajax(url, element, cb) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById(element).innerHTML = this.responseText;
      cb();
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}