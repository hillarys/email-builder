//------ Duplicates and moves all images on build ------//

import gulp from "gulp";
import newer from "gulp-newer";
import del from "del";
import color from "gulp-color";
import { loopFolders, getConfigFile, getPaths } from "../helpers/helpers";

const imageType = 'gif,jpg,png,svg';

// Public methods

export function updateImageFolder(type, folderPath) {
    console.log(color(`Detected image change in ${folderPath}, updating images`, "GREEN"));
    let pathArray = folderPath.split("/");
    let folder = pathArray[1];

    deleteImageFolder(folder);
    moveImageFolder(folder);
}

export function updateAllImages(cb){
    loopFolders(getPaths().tmpPath, deleteImageFolder);    // Deletes all existing image folders
    loopFolders(getPaths().tmpPath, moveImageFolder);      // Copies all images to tmp

    cb();
}


// Private methods 

function deleteImageFolder(folder){
    const imageFolder = `${getPaths().tmpPath}/${folder}/*.{${imageType}}`;
    return del(imageFolder);
}

function moveImageFolder(folder){
    const srcImageFolder = `${getPaths().srcPath}/${folder}/images`,
     config = getConfigFile(folder),
     currentBrand = config.project.brand.toLowerCase(),
     brandImageFolder = `./builder/_email/brands/${currentBrand}/images`;

     return gulp.src([
       `${srcImageFolder}/*.{${imageType}}`,
       `${brandImageFolder}/*.{${imageType}}`
     ])
     .pipe(newer(`${getPaths().tmpPath}/${folder}`))
     .pipe(gulp.dest(`${getPaths().tmpPath}/${folder}`));
}