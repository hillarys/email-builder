import gulp from "gulp";
import handlebars from "gulp-compile-handlebars";
import rename from "gulp-rename";
import notify from "gulp-notify";

import {
  loopFolders,
  getConfigFile,
  getPaths
} from "../helpers/helpers";

// Public methods

export function compileHandlebarsFile(folderPath) {

  let pathArray = folderPath.split("/");
  let folder = pathArray[1];

  compileHandlebars(folder);
}

export function compileAllHandlebars(cb) {
  loopFolders(getPaths().srcPath, compileHandlebars); // Compiles all handlebars files
  cb();
}

// Private methods 
function compileHandlebars(folder) {

  let config = getConfigFile(folder),
    brand = config.project.brand.toLowerCase();

  // Import dynamic modules
  let moduleArray = [];

  loopFolders("./builder/_email/components", function(folder){
    moduleArray.push(`./builder/_email/components/${folder}`);
  });

  moduleArray.push("./builder/_email/containers");
  moduleArray.push(`./builder/_email/brands/${brand}/blocks`);
  moduleArray.push(`./builder/_email/brands/${brand}/meta`);
  moduleArray.push(`./builder/_email/brands/${brand}/styles`);
  moduleArray.push(`./builder/_email/brands/${brand}/templates`);
  moduleArray.push(`${getPaths().srcPath}/${folder}/styles`);

  let options = {
    ignorePartials: false,
    batch: moduleArray,
    helpers: {
      checkMobileSpacer: function(string){
        switch(string) {
          case "large":
            return "30"
          case "medium":
            return "20"
          case "small":
            return "10"       
          default:
            return "0"
        }
      },
      math: function (lvalue, operator, rvalue) {
        lvalue = parseFloat(lvalue);
        rvalue = parseFloat(rvalue);
        return {
          "+": lvalue + rvalue,
          "-": lvalue - rvalue,
          "*": lvalue * rvalue,
          "/": lvalue / rvalue,
          "%": lvalue % rvalue
        } [operator];
      },
      // Removes conditional space around first and last nav items //
      navConditionalSpace: function(index, string, length, size){
        
        let spaceChar = '&nbsp;';
        let space = '';

        var i;
        for (i = 0; i < size; i++) { 
          space = space + spaceChar;
        }

        if(index + 1 == length){
          return `${space}${string}`
        } else if (index == 0){
          return `${string}${space}`
        } else {
          return `${space}${string}${space}`
        }
      },
      // Makes sure widths are right per grid column, negating any padding or gutter space //
      calculateColumnSize: function(columns){
        columns = columns ? columns : 1;
        var emailWidth = 680;
        var calc = emailWidth / columns;
        return Math.floor(calc);
      },
      // Character replacer for suspect characters in older clients
      escapeCharacter: function(string){
        var replaceChar = [
          {reg : '&', replace: '&amp;'},
          {reg : '"', replace: '&quot;'},
          {reg : '£', replace: '&pound;'},
          {reg : '€', replace: '&euro;'},
          {reg : 'é', replace: '&eacute;'},
          {reg : '–', replace: '&ndash;'},
          {reg : '®', replace: '&reg;'},
          {reg : '™', replace: '&trade;'},
          {reg : '‘', replace: '&lsquo;'},
          {reg : '’', replace: '&rsquo;'},
          {reg : '“', replace: '&ldquo;'},
          {reg : '”', replace: '&rdquo;'},
          {reg : '#', replace: '&#35;'},
          {reg : '©', replace: '&copy;'},
          {reg : '\\(', replace: '&#40;'},
          {reg : '\\)', replace: '&#41;'},
          {reg : '<', replace: '&lt;'},
          {reg : '>', replace: '&gt;'},
          {reg : '…', replace: '&hellip;'},
          {reg : '-', replace: '&#45;'},
          {reg : "'", replace: '&#39;'},
          {reg : '\\*', replace: '&#42;'},
          {reg : ',', replace: '&sbquo;'}
        ];

        // Finds and replaces all special characters
        var s = string;
        replaceChar.forEach(function(obj){
          s = s.replace(new RegExp("(<[^<>]*>)|" + obj.reg.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "g"), function ($0,$1) { return $1 ? $0 : obj.replace } );
        });

        // Adds a non-breaking space if words in a sentence is more than 3
        var sentenceArray = s.split(" ");
        if(sentenceArray.length >=3){
          var noWidowString = "";
          for (var i=0;i<=sentenceArray.length-1;i++) {
            noWidowString += sentenceArray[i];
             if (i == (sentenceArray.length-2)) {
              noWidowString += "&nbsp;";
             } else { 
              noWidowString += " ";
            }
          }
          s = noWidowString;
        }
        return s
      },
      addChevron: function(size){
        let s = `<span style="font-size: ${size};">&nbsp;&rsaquo;</span>`
        return s
      },
      combine: function(arg1, arg2){
        let s = `${arg1}${arg2}`;
        return s
      }
    }
  };

  return gulp
    .src(`${getPaths().srcPath}/${folder}/index.hbs`)
    .pipe(handlebars(config, options))
    .on('error', function (err) {
        notify.onError({
            title: "Email Builder Error!",
            message: err.message
        })(err);
        this.emit('end');
    })
    .pipe(rename(`index.html`))
    .pipe(gulp.dest(`${getPaths().tmpPath}/${folder}`))
}