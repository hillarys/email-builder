// Task - Creates a config file

import fs from "file-system";
import { loopFolders, getPaths } from "../helpers/helpers";

export function createConfigFile(cb) {
  // Merges multiple JSON files to create one file
  function mergeConfigFiles(folder) {
    const srcFolder = `${getPaths().srcPath}/${folder}`;

    // Read and store all json files
    const srcConfig = JSON.parse(
      fs.readFileSync(`${srcFolder}/config/project.json`)
    ),
      advancedConfig = JSON.parse(
        fs.readFileSync(`${srcFolder}/styles/advanced-styles.json`)
      ),
      brandConfig = JSON.parse(
        fs.readFileSync(
          `./builder/_email/brands/${srcConfig.project.brand.toLowerCase()}/styles/styles.json`
        )
      )

    // Use object assign to merge files
    let json = Object.assign(
      {},
      srcConfig,
      advancedConfig,
      brandConfig
    );

    // Writes file to tmp file
    fs.writeFileSync(
      `${getPaths().tmpPath}/${folder}/.data/data.json`,
      JSON.stringify(json)
    );
  }

  loopFolders(getPaths().srcPath, mergeConfigFiles);

  cb();
}
