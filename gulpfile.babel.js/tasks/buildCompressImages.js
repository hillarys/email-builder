//------ Optimizes all images on build ------//
// TODO Not async

import gulp from "gulp";
import imagemin from "gulp-imagemin";
import newer from "gulp-newer";
import color from "gulp-color";
import imageminGuetzli from "imagemin-guetzli";
import { loopFolders, getConfigFile, getPaths } from "../helpers/helpers";

export function compressImages(cb) {
    console.log(color("Compressing all images", "GREEN"));

    function compressImageFolder(folder){
      console.log(color(`Compressing images in folder: ${folder}`, "GREEN"));
      
      let config = getConfigFile(folder)
      if(config == 'undefined'){
        cb();
      }

        gulp
        .src(`${getPaths().tmpPath}/${folder}/*.{gif,jpg,png,svg}`)
        .pipe(newer(`${getPaths().distPath}/${folder}`))
        .pipe(
          imagemin([
            imagemin.gifsicle({
              interlaced: true
            }),
            imagemin.optipng({
              optimizationLevel: 85
            })
            // imageminGuetzli({
            //     quality: config.imageOptimizing.optimizationLevel
            // })
          ])
        )
        .pipe(gulp.dest(`${getPaths().distPath}/${folder}`));
    }

    loopFolders(getPaths().srcPath, compressImageFolder);

    cb();

  }