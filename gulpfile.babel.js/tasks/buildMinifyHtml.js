// Minifies, Removes empty tags, and formats the HTML on final build

import gulp from "gulp";
import color from "gulp-color";
import htmlmin from "gulp-htmlmin";
// import htmlmin from "gulp-html-minifier";
import format from "gulp-format-html";
import replace from "gulp-replace";

// Added format as an alternative to minify //

import { loopFolders, getConfigFile, getPaths } from "../helpers/helpers";

// Email safe options for minify
const options = {
  collapseWhitespace: true,
  conservativeCollapse: true,
  removeComments: false,
  sortClassName: true,
  sortAttributes: true,
  sortClassName: true,
  minifyCSS: false,
  processConditionalComments: false,
  removeEmptyAttributes: true,
  removeRedundantAttributes: true,
  keepClosingSlash: true,
  html5: false
};

export function minifyHTML(cb) {
  loopFolders(getPaths().srcPath, minifyFile);
  cb();
}

function minifyFile(folder){

  let config = getConfigFile(folder);

  return gulp.src(`${getPaths().tmpPath}/${folder}/index.html`)
  .pipe(htmlmin(options))
  .pipe(format({"end_with_newline": true}))
  // TO-DO - This is supposed to add a .ie or .co.uk but is failing - This is causing an /Undifined to appear on build


  // .pipe(replace('hillarys.co.uk', function(match, p1, offset, string) {
  //   // Checks if regional settings are Hillarys IE, and updates all references of hillarys.co.uk to hillarys.ie
  //   if(config.project.baseUrl == "https://www.hillarys.ie"){
  //       return 'hillarys.ie'
  //     }
  //     return 'hillarys.co.uk'
  //   }))
  //   .pipe(replace('hillarys.ie', function(match, p1, offset, string) {
  //     // Same but for i.e.
  //     if(config.project.baseUrl == "https://www.hillarys.co.uk"){
  //       return 'hillarys.co.uk'
  //     }
  //     return 'hillarys.ie'
  //   }))
    // .pipe(replace(/href=['"]([^'"]+?)['"]/g, function(match, p1, offset, string) {
    //   // Basically adds the correct tracking to each a href link - removes any trailing slashes from original links
    //   var url = p1;
    //   if (url.substring(url.length-1) == "/")
    //   {
    //     url = url.substring(0, url.length-1);
    //   }
    //   if(url == "https://twitter.com/hillarysblinds"){
    //     url = `${config.tracking.prefix}${url}/${config.tracking.suffix}`
    //   } else {
    //     url = `${config.tracking.prefix}${url}/${config.tracking.suffix}${config.tracking.nameurn}`
    //   }
    //   return `href="${url}"`;
    // }))
  .pipe(gulp.dest(`${getPaths().distPath}/${folder}`))
};