// ---- Cleans tmp file on start ----//
import del from "del";
import color from "gulp-color";
import { getPaths } from "../helpers/helpers";

export function cleanTmp(cb) {
    console.log(color("Cleaning temp folders", "GREEN"));
    return del(getPaths().tmpPath);
    cb();
}

export function cleanDist(cb) {
    console.log(color("Cleaning dist folders", "GREEN"));
    return del(getPaths().distPath);
    cb();
}