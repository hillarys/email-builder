import gulp from "gulp";
import fs from "file-system";
import { getFolders, getPaths } from "../helpers/helpers";

export function createIndex() {
    
    let htmlString = "";

    getFolders(getPaths().srcPath).forEach(function(folder, index) {
      htmlString += `<li class="email-link" data-link="${folder}">${folder}</li>`;
    });
    
    fs.writeFileSync(`builder/.docs/directory/links.html`, htmlString);

    return gulp
    .src('builder/.docs/directory/**/*')
    .pipe(gulp.dest(`${getPaths().tmpPath}`));
}
