// ---- Creates a new folder based on  ----//

import gulp from "gulp";
import color from "gulp-color";
import { getPaths } from "../helpers/helpers";

export function createEmail() {
    console.log(color("Creating new folder...", "GREEN"));

    let folderName = process.argv[process.argv.length-1];
    // Add in a breaker here to add the file name within terminal
    if(folderName == "create"){
        folderName = `newEmail-${new Date().valueOf()}`;
    } else {
        folderName = folderName.substring(2);
    }

    return gulp
    .src('builder/.docs/blankTemplate/**/*')
    .pipe(gulp.dest(`${getPaths().srcPath}/${folderName}`));
}