// Zips distribution files
// TODO - currently not working async after image optimising


import gulp from "gulp";
import zip from "gulp-zip";
import color from "gulp-color";
import { loopFolders, getPaths } from "../helpers/helpers";

export function zipFiles(cb) {
  console.log(color("Zipping folders...", "GREEN"));

  function zipFile(folder) {
    gulp
      .src(`${getPaths().distPath}/${folder}/*`)
      .pipe(zip(`${folder}.zip`))
      .pipe(gulp.dest(`${getPaths().distPath}`));
  }

  loopFolders(getPaths().srcPath, zipFile);

  cb();
}
