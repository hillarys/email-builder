import fs from 'file-system';
import path from 'path';
import yaml from 'js-yaml';

const tmpPath = getPaths().tmpPath;

// Helper - gets current structure of dir file
export function getFolders(dir) {
  return fs.readdirSync(dir).filter(function (file) {
    return fs.statSync(path.join(dir, file)).isDirectory();
  });
}

// Helper - loop folders an executes a function
export function loopFolders(dir, fn) {
  let folders = getFolders(dir);
  if (folders.length === 0) return done();

  return folders.map(function (folder) {
    fn(folder);
  });
}

// Helper - returns an object with relative paths
export function getPaths() {

  var settings = yaml.safeLoad(fs.readFileSync('settings.yaml', 'utf8'));

  return {
    srcPath: settings.srcFolder,
    distPath: settings.distFolder,
    tmpPath: settings.tmpFolder
  }
}

// Helper - returns config file as js object
export function getConfigFile(folder) {
  if (folder !== 'assets') {
    let configPath = `${tmpPath}/${folder}/.data/data.json`,
      config = JSON.parse(fs.readFileSync(configPath));
    return config;
  } else {
    return
  }
}