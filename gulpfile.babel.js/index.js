// Gulp
import gulp from "gulp";
import browsersync from "browser-sync";
import { getPaths } from "./helpers/helpers";

// Development
import { cleanTmp, cleanDist } from "./tasks/devClean";
import { createConfigFile } from "./tasks/devConfigFiles";
import { compileHandlebarsFile, compileAllHandlebars } from "./tasks/devCompileHandlebars";
import { updateImageFolder, updateAllImages } from "./tasks/devMoveImages";
import { createEmail } from './tasks/devCreateEmail';
import { createIndex } from "./tasks/devCreateIndex";

// Build
import { compressImages } from "./tasks/buildCompressImages";
import { minifyHTML } from "./tasks/buildMinifyHtml";
import { zipFiles } from "./tasks/buildZipFiles";

// Test
// import { sendEmail } from "./tasks/testEmail";
// import { testLitmus } from "./tasks/testLitmus";
// import { validateHtml } from "./tasks/testValidateHtml";

browsersync.create();

// Browser Sync for Live Reloading //
const tmpPath = getPaths().tmpPath

function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: tmpPath
    },
    port: 3000
  });
  done();
}

// BrowserSync Reload (callback)
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Watchers
function watchFiles(cb) {

  // Watch paths
  const configFiles = [
  "./builder/_email/brands/**/styles/*.json",
  "./src/**/styles/*.json",
  "./src/**/config/*.json"
  ],
  srcImagePath = "./_src/**/images/*.*",
  srcHbsPath = "./_src/**/*.hbs",
  brandImagePath = "./builder/_email/brands/**/images/*.*",
  brandHbsPath = "./builder/_email/brands/**/**/*.hbs",
  componentHbsPath = "./builder/_email/components/**/**/*.hbs";
  
  // Watches for any changes to brand imagery
  gulp.watch(brandImagePath, updateAllImages);

  // Watches for changes to template hbs files and updates
  gulp.watch(brandHbsPath, compileAllHandlebars);

  // Watches for changes to the image files
  gulp.watch(srcImagePath).on("all", updateImageFolder);

  // Watches for changes to the hbs file and passes the path
  gulp.watch(srcHbsPath, { ignoreInitial: false }).on("change", compileHandlebarsFile);

  // TO DO - Watch component files update and run compileHandlebars
  // gulp.watch(srcHbsPath, { ignoreInitial: false }).on("change", compileHandlebarsFile);

  // Watches for changes to the config files
  gulp.watch(configFiles, createConfigFile);

  // Reload on any change
  gulp.watch([srcImagePath, brandImagePath, componentHbsPath, brandHbsPath, srcHbsPath].concat(configFiles), browserSyncReload);
  
  cb();
}

// Process
const create = gulp.series(createEmail);
const start = gulp.series(cleanTmp, gulp.parallel(createConfigFile, updateAllImages), compileAllHandlebars, createIndex, gulp.parallel(watchFiles, browserSync));
const build = gulp.series(cleanDist, gulp.parallel(compressImages, minifyHTML), zipFiles);
const zip = gulp.series(zipFiles);

// const send = gulp.series()
// const litmus = gulp.series(testLitmus);

// Exposed gulp tasks
exports.start = start;
exports.create = create;
exports.build = build;
exports.zip = zip;
exports.default = start
