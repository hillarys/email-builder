# Hillarys Email Boilerplate #

Hillarys uses an advanced email boilerplate to ensure all emails are built efficiently with minimal risk of human error. To get started, clone the git repository and follow the terminal commands below.


## Installation ##

*Please clone this folder per campaign / individual email*

To use Email builder you will need to:
Install Node - https://nodejs.org/en/download/

Create a folder within your documents called emails and create a specific folder for your email campaign.

Open terminal on your Mac.

Type cd, then put a space and drag the campain folder from finder window into terminal. This will add the email folder directory into the terminal window. It should look like this ```cd (path to folder)``` and hit enter.

Then copy this ```git clone https://elliothillarys@bitbucket.org/hillarys/email-builder.git``` paste into terminal and hit enter.

This will download the email builder code into your email folder.

Once this has finished downloading type cd, then put a space and drag the newly downloded email-builder folder into terminal. It should look like this in terminal ```cd (path to email-builder folder)``` and hit enter.

If this is your first time using email builder type ```make``` and hit enter. Otherwise type ```npm install``` to install the required files to run email builder.

## Terminal Commands ##

Starts the local server to view emails in browser.

```gulp
gulp
```

Tests the build in Litmus on chosen clients. The client list is specificially defined for Hillarys clients, update this if needed. All Litmus confidential details are defined on the shared drive, not in this repository (for security reasons).

*Please note, after each campaign please place a reference of this project in the Hillarys job folder. This does not overwrite the existing Hillarys job process.*

## Building Emails ##

To build emails use a code editor. I recommend Visual Studio Code.

Download VS Code - https://code.visualstudio.com/

Drag the email-builder folder from Finder into VS Code.

The emails will be within the _src folder.

You can find the example emails (all-components, new-components and ts-test) in the _src folder. These display the correct structure for emails in the handlebar.js coding language.

If you are building a Thomas Sanderson email remove the all-components and the new-components folders from _src

If you are building a Hillarys email remove the ts-test folder from _src

## Creating your first email ##
For a basic email you will only need to reference the src and dist files. The examples folder can be used to fast-track a project or be used for inspiration. We will always keep a reference of our best performing emails and campaigns in the folder.

+ To get started, locate the index.hbs file within the src folder - this is the meat and bones of your email. Run the gulp start command to get started.

+ Your email is wrapped in a template tag. Ensure the wrapper has the correct handlebars syntax, and select the template you want to use. Below we're using the hillarys standard template by default.

```handlebars
{{!-- Start of Email Wrapper --}}
{{#> blank }}

{{!-- Your email code goes here! --}}

{{/blank}}
{{!-- End of Email Wrapper --}}
```

+ Once you're happy with the way your email looks in email-builder you need to export the code to Litmus to test and send a test email. 

Collect your email from the ```builder``` - ```.tmp``` folder.


## Making general fixes / updates ##
All of the components and templates that build the email can be found within the builder/_email folder structure. If you update this folder, please ensure you also update any snippets.


## Updating the basic config file ##
Each email will have a config/project.json config file associated with it. Update this to update basic email information, including tracking, phone number, email name, etc. This should be done for each email.


## Changing or creating components ##

To create or edit an existing component, please locate the _email folder. Here you will find a list of components and blocks that make up the email syntax. This is raw HTML code built in tables to ensure email clients render these properly. If you want to add a component, create a new handlebars file here. Try to restrict styling to the CSS section, as this is injected using inlining during the build phase. Please render on all clients thoroughly before requesting changes be made to the mater template.


## Changing or creating a theme ##

To change or create a template, refer to the _templates folder within the _email folder structure. Here you will find a list of templates and styles/themes for each brand. If you want to add a new theme, place it here. Please don't overwrite or delete existing files unless you are close to the system.

## Useful Links ##

+ [Handlebars.js](https://handlebarsjs.com/)
+ [CSS Tricks](https://css-tricks.com/)
+ [Litmus](https://litmus.com/dashboard)
+ [Legacy Emails](http://projects.hillarys.co.uk/email-library/)
+ [Email Bug List](https://promotions.studygroup.com/promotions/_Email_template_2017/Guidance/bugs-list.html)
+ [Hillarys Email Tracking](https://docs.google.com/spreadsheets/d/1F5mBMnXD5dKPe86Zb9h5B-NH5B60CYpR4oaaEmZauPM/)
+ [Thomas Sanderson Email Tracking](https://docs.google.com/spreadsheets/d/1fi3WlxR7TSeCuqC9Odm_86nNVtYAojwZl1JugoeU44k/)
+ [Email QA Checklist](https://docs.google.com/spreadsheets/d/1rl3OZY3a5V4YetFXRxNk-3_WlCEt8Se5B1p8zcJK_Cs)