HR = \n---------------------------------------

greenOpen = \033[32m
redOpen = \033[31m
close = \033[39m

CHECK = ${greenOpen}✓${close}
CROSS = ${redOpen}✗${close}

BUILD := build

GULP     = $(shell which gulp)
NODE     = $(shell which node)
NPM      = $(shell which npm)
SASS     = $(shell which sass)
SASSLINT = $(shell which scss-lint)

build:

# Install Node
ifeq (${NODE}, )
	@ echo "${HR}\nInstalling Node...${HR}\n"
	brew install node
	sudo npm install -g npm
else
	@ echo "${greenOpen}Node${close} is already installed."
endif

# Install sass
ifeq (${SASS}, )
	@ echo "${HR}\nInstalling Sass and its dependencies...${HR}\n"
	@ sudo gem install sass -v 3.4.22
else
	@ echo "${greenOpen}Sass${close} is already installed."
endif

# Install scss_lint
ifeq (${SASS}, )
	@ echo "${HR}\nInstalling scsslint...${HR}\n"
	@ sudo gem install scss_lint -v 0.50.2
else
	@ echo "${greenOpen}Scss-Lint${close} is already installed."
endif

# Install Gulp
ifeq (${GULP}, )
	@ echo "${HR}\nInstalling Gulp...${HR}\n"
	@ sudo npm install -g gulp-cli
else
	@ echo "${greenOpen}Gulp${close} is already installed."
endif

# Install NPM dependencies
	@ echo "${HR}\nInstalling packages...${HR}"
	@ npm install

# Run gulp workflow
	@ echo "${HR}\nRunning the first build${HR}"
	@ echo "${greenOpen}gulp start${close}${HR}"
	@ gulp start
	@ echo "${HR}\n${CHECK} Done\n"

clean:
	@ echo "${HEADER}"
	@ echo "${HR}\nRemoving front-end libraries...${HR}"
	@ rm -rf node_modules/
	@ echo "\n${CHECK} Done\n"
	@ echo "${HR}\nVerifying cache and garbage collection${HR}"
	@ npm cache verify
	@ echo "\n${CHECK} Done\n"

version:
	@ echo "${HR}"
	@ echo "SASS         $(shell sass --version) ${HR}"
	@ echo "SCSS-LINT    $(shell scss-lint --version) ${HR}"
	@ echo "GULP         $(shell gulp --version) ${HR}"
	@ echo "NODE         $(shell node --version) ${HR}"
	@ echo "NPM          $(shell npm --version) ${HR}"
